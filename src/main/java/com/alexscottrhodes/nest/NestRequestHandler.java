package com.alexscottrhodes.nest;

import java.util.HashSet;
import java.util.Set;
import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

public final class NestRequestHandler extends SpeechletRequestStreamHandler {
    private static final Set<String> supportedApplicationIds = new HashSet<String>();
    static {
    	//Actual:
        supportedApplicationIds.add("<Amazon application ID here>");

    }

    public NestRequestHandler() {
        super(new NestSpeechlet(), supportedApplicationIds);
    }
}

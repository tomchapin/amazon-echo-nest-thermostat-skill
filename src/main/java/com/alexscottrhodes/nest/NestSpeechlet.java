package com.alexscottrhodes.nest;

import com.alexscottrhodes.actions.NestFunctions;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.Speechlet;
import com.amazon.speech.speechlet.SpeechletException;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.speechlet.User;
import com.amazon.speech.ui.LinkAccountCard;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;
import com.amazon.speech.ui.SimpleCard;

public class NestSpeechlet implements Speechlet{

	//private static final Logger log = LoggerFactory.getLogger(EricaSpeechlet.class);
	
	public void onSessionStarted(SessionStartedRequest request, Session session) throws SpeechletException {
		//Initialization functions
	}

	public SpeechletResponse onLaunch(LaunchRequest request, Session session) throws SpeechletException {
		//Initial greeting/launch
		//log.info("onLaunch requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());
		return getGreetingResponse();
	}

	public SpeechletResponse onIntent(IntentRequest request, Session session) throws SpeechletException {
		Intent intent = request.getIntent();
		String intentName = (intent != null) ? intent.getName() : null;

		
		if ("AMAZON.HelpIntent".equals(intentName)) {
			return getHelpResponse(); 
		}else if("AMAZON.CancelIntent".equals(intentName)){
			return exitResponse();
		}
		else if("GreetingIntent".equals(intentName)){
			return getGreetingResponse();
		}else {
			return NestFunctions.InitiateResponse(intent);
		}
	}

	public void onSessionEnded(SessionEndedRequest request, Session session) throws SpeechletException {
		//At the end of a session
	}
	
	
	//Core logic 
	
	//Initial greeting
	private SpeechletResponse getGreetingResponse() {
		String speechText = "Hello, I'm going to help Alexa control your Nest thermostat. How can I help you?";


		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);

		// Create reprompt
		Reprompt reprompt = new Reprompt();
		reprompt.setOutputSpeech(speech);

		return SpeechletResponse.newAskResponse(speech, reprompt);
	}
	
	private SpeechletResponse exitResponse() {
		String speechText = "Goodbye.";


		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		SpeechletResponse sr = new SpeechletResponse();
		sr.setOutputSpeech(speech);
		sr.setShouldEndSession(true);
		return sr;
	}
	
	//Help response
	private SpeechletResponse getHelpResponse() {
		String speechText = "Thermostat can control your temperature and mode settings, or tell what your Nest is doing. You can say things like: set the temperature to seventy one degrees, switch to heating mode. How can I help you?";

		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);

		// Create reprompt
		Reprompt reprompt = new Reprompt();
		reprompt.setOutputSpeech(speech);

		return SpeechletResponse.newAskResponse(speech, reprompt);
	}
	

}

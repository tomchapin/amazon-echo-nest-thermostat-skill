# Amazon Echo/Nest Learning Thermostat Control #
This source code is for an Amazon Echo "Alexa" skill to control the Nest Learning Thermostat. It was previously under the Amazon Skills and Works With Nest stores. Nest has released native integration with the Amazon Echo. I am releasing this skill because it will no longer be supported. It offers more features than the Nest version. If you are into using custom skill/alexa you can use this skill for your Echo.

##Set Up
###Requirements
* A Nest application (free from nest developer site) that allows you to get API access to your nest. 
* General knowledge of Amazon Skill dev process. 

Without going into detail about skill development etc.. This skill can be hosted in a Lambda function on AWS as typical of any skill.  It requires that you enter the Alexa Skill Application ID in the request handle, and your Nest API Key in the nest functions class. Then you need to add a default thermostat ID that you want to use when you DO NOT specify a thermostat name. If you specify a thermostat name, it will get the ID from your home structure. (See below for supported commands etc.) 

##Skill description
Full description of supported features here:
http://alexsrhodes.webfactional.com/misc/therm_legacy/echo_help.html

##Contact##
Please email me with any questions, I'm not going to guide you through skill development or using the Nest application etc. Tutorials are readily available for that on Amazon and Nest's developer sites.
Email: alexscottrhodes@gmail.com